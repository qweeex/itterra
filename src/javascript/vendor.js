$(document).ready(app());

function app() {
    $.exists = function(selector) {
        return ($(selector).length > 0);
    };
    productSlider();
    ajaxForm();
    countRange();
    partnersTab();
    if ($.exists('.production-slider')){

        /*$('.production-play').on('click', function () {

            let elem = $(this);
            let slide = elem.parent();
            let _id = slide[0].id;
            let _video = slide[0].dataset.video;


            jQuery("#slide1").YTPlayer();


        });*/

    }
}
function productSlider() {

    let active = 'product-item__active';
    let max = $('.product-item').length - 1;
    let min = 0;

    $('.product-item').on('click', (e) => {
        $('.product-item').removeClass(active);
        e.target.classList.add(active);
        changeInfo(
            e.target.dataset.cup,
            e.target.dataset.title,
            e.target.dataset.link,
            e.target.dataset.material,
            e.target.dataset.avalible,
            e.target.dataset.picture
        );
    });

    function changeInfo(cup, title, link, material, avalible, img) {
        let materialText = $('.content-material > p > span').text();
        let avalibleText = $('.content-available > p > span').text();
        $('.content-material').html(`
            <p><span>${materialText}</span> ${material}</p>
        `);
        $('.content-available').html(`
            <p><span>${avalibleText}</span> ${avalible}</p>
        `);
        $('.content-more > a').attr('href', link);
        $('.product-simple > img').attr('src', img);
        $('.product-simple > .simple-title').text(cup);
        $('.content-warning p').text(title);
    }

    $('.product-up').on('click', function (e) {
        e.preventDefault();
        slideNext();
    });
    $('.product-down').on('click', function (e) {
        e.preventDefault();
        slidePrev();
    });


    function slideNext() {
        let active = 'product-item__active';
        let slides = $('.product-item');

        for (let i = 0; i < slides.length + 1; i++){
            min = i + 1;
            if (slides[i].classList.contains(active)){
                if (min > max){
                    min = 0;
                    slides[max].classList.remove(active);
                    slides[0].classList.add(active);
                    changeInfo(
                        slides[0].dataset.cup,
                        slides[0].dataset.title,
                        slides[0].dataset.link,
                        slides[0].dataset.material,
                        slides[0].dataset.avalible,
                        slides[0].dataset.picture
                    );
                    return;
                } else {
                    slides[i].classList.remove(active);
                    slides[i + 1].classList.add(active);
                    changeInfo(
                        slides[i + 1].dataset.cup,
                        slides[i + 1].dataset.title,
                        slides[i + 1].dataset.link,
                        slides[i + 1].dataset.material,
                        slides[i + 1].dataset.avalible,
                        slides[i + 1].dataset.picture
                    );
                    return;
                }
            }
        }

    }

    function slidePrev() {
        let active = 'product-item__active';
        let slides = $('.product-item');
        for (let i = 0; i < slides.length + 1; i++){
            //min = i - 1;
            if (slides[0].classList.contains(active)) {
                min = max;
                slides[0].classList.remove(active);
                slides[max].classList.add(active);
                changeInfo(
                    slides[max].dataset.cup,
                    slides[max].dataset.title,
                    slides[max].dataset.link,
                    slides[max].dataset.material,
                    slides[max].dataset.avalible,
                    slides[max].dataset.picture
                );
                return;
            }
            min = min - 1;
            slides[min + 1].classList.remove(active);
            slides[min].classList.add(active);
            changeInfo(
                slides[min].dataset.cup,
                slides[min].dataset.title,
                slides[min].dataset.link,
                slides[min].dataset.material,
                slides[min].dataset.avalible,
                slides[min].dataset.picture
            );
            return;
        }
    }

}

function ajaxForm() {

    $(document).on('click', '.form-submit__btn', function () {

        let name = $('input[name="name"]');
        let phone = $('input[name="phone"]');
        let email = $('input[name="email"]');
        let message = $('textarea[name="message"]');
        let company = $('input[name="company"]');

        $('input').on('focus', function () {
            $(this).removeClass('error');
        });

        if (name.val() === '' || name.val().length < 3){
            name.addClass('error');
        }
        if (phone.val() === '' && phone.val().length < 3){
            phone.addClass('error');
            return;
        }

        $.ajax({
            url: '/uploads/index.php',
            type: 'POST',
            data: {
                'name': name.val(),
                'phone': phone.val(),
                'message': message.val(),
                'email': email.val(),
                'company': company.val()
            },
            success: function(response) {
                $('#ex4 .modal-title p').text('Thank you for your request');
                $('#ex4').modal();
                $('.form-val').val('');
                console.log('good');
            },
            error: function(response) {
                $('#ex4 .modal-title  p').text('Something went wrong');
                $('#ex4').modal();
                console.log('bad')
            }
        });

    });

}

function countRange() {

    $('.card-range__input').ionRangeSlider({
        skin: "round",
        prettify_separator: '.',
        hide_min_max: true
    });

}

function partnersTab() {

    $('.tab-btn').on('click', function (e) {
        let elem = $(this);
        $('.tab-btn').removeClass('tab-btn__active');
        elem.addClass('tab-btn__active');
        $('.production-certificate').addClass('d-none');
        let data = elem.data('tab');
        $('.production-certificate[data-tab='+data+']').removeClass('d-none');

    })

}
