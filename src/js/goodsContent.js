import $ from "jquery";

class GoodsContent {
    constructor() {
        this.height = document.querySelector('.goods-content__text').clientHeight;
    }
    init() {
        $('.goods-content__btn button').on('click', (e) => {
            this.readMore(e);
        });
        $('.content-tab').on('click', (e) => {
            this.selectTab(e)
        })
    }
    readMore(elem) {
        if (!elem.target.classList.contains('read-show')){
            elem.target.classList.add('read-show');
            $('.goods-content__text').css('height', 'auto').addClass('no-before');
        } else {
            elem.target.classList.remove('read-show');
            $('.goods-content__text').css('height', this.height).removeClass('no-before');
        }
    }

    selectTab(elem) {
        $('.content-tab').removeClass('tab-active');
        elem.target.classList.add('tab-active');
        let id = elem.target.dataset.content;
        $('.good-tab__content').addClass('d-none');
        $('.good-tab__content[data-content="'+id+'"]').removeClass('d-none');
        if (elem.target.dataset.content === 'app'){
            $('.goods-content__text').css('height', this.height).removeClass('no-before');
        } else {
            $('.goods-content__text').css('height', '100%').addClass('no-before');
        }
    }

}

export default GoodsContent;
