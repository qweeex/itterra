import $ from 'jquery';
import Quality from "./quality";
import Swiper from 'swiper';
require('jquery-modal');
import GoodsContent from './goodsContent';
window.$ = $;
window.jQuery = $;
window.YTPlayer = require('jquery.mb.ytplayer');
$(document).ready(app());

function app() {
    let inputMask = new Inputmask('+421(999)999-9999');
    inputMask.mask('input[name="phone"]');
    $.exists = function(selector) {
        return ($(selector).length > 0);
    };
    $('.header-search__btn').on('click', function (e) {
        e.preventDefault();
        $('.search-row').addClass('search-row__show');
        $('.header-search__input input').focus();
    });
    $('.header-search__close .close-btn').on('click', function () {
        $('.search-row').removeClass('search-row__show');
    });
    $('.modal').modal({
        showClose: false,
        clickClose: true,
        closeExisting: true
    });
    $('.btn-option').on('click', function (e) {
        let id = e.target.dataset.weight;
        $('.btn-option').removeClass('btn-option__active');
        $(this).addClass('btn-option__active');
        $('.cup-img').addClass('d-none');
        $('.cup-img[data-weight='+id+']').removeClass('d-none');
    });
    if ($.exists('.product')){
        let quality = new Quality();
        quality.init();
        $(window).resize(function () {
            quality.init();
        });
    }

    let slider = new Swiper('.slider', {
        autoplay: {
            delay: 5000,
        },
        pagination: {
            el: '.slider-dots__wrapper',
            type: 'bullets',
            bulletActiveClass: 'dots-active',
            bulletClass: 'dots',
            clickable: true
        },
        renderBullet: function (index, className) {
            return '<div class="' + className + '"></div>';
        }
    });
    if ($.exists('.wordpage-slider')){
        let about = new Swiper('.wordpage-slider', {
            slidesPerView: 3,
            centeredSlides: true,
            breakpoints: {
                300: {
                    slidesPerView: 1
                },
                795: {
                    slidesPerView: 3
                }
            }
        });

        about.slideTo(1);
        $(document).on('click', '.wordpage-arrow_left', function () {
            about.slidePrev();
        });
        $(document).on('click', '.wordpage-arrow_right', function () {
            about.slideNext();
        });
    }
    $('.header-mobile__btn').on('click', function (e) {
        e.preventDefault();
        if (!$('.header-mobileMenu').hasClass('header-mobile__active')){
            $('.header-mobileMenu').addClass('header-mobile__active');
            $('.header-mobile__btn > img').attr('src', '../../img/close-menu.svg');
        } else {
            $('.header-mobileMenu').removeClass('header-mobile__active');
            $('.header-mobile__btn > img').attr('src', '../../img/menu.svg');
        }
    });
    if ($.exists('.goods')){
        let goods = new GoodsContent();
        goods.init();
    }
    // Scroll top
    $(window).scroll(function () {
        if ($(this).scrollTop() > 0) {
            $('.arrow-up').removeClass('d-none');
        } else {
            $('.arrow-up').addClass('d-none');
        }
    });
    $('.arrow-up__btn').click(function (e) {
        e.preventDefault();
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });
    if ($.exists('.production-slider')){
        let production = new Swiper('.production-slider', {
            autoplay: {
                delay: 5000,
            },
            pagination: {
                el: '.production-dots__wrapper',
                type: 'bullets',
                bulletActiveClass: 'dots-active',
                bulletClass: 'dots',
                clickable: true
            },
            renderBullet: function (index, className) {
                return '<div class="' + className + '"></div>';
            }
        });
        $('.production-play').on('click', function () {
            let el = $(this);
            let id = el.parent()[0].id;
            let video = $('#' + id + ' iframe');
            video[0].style.display = 'block';
            video[0].src += '?autoplay=1';
        });

    }
}

